const scriptParser = require('./scriptParser');
const parser = require('./parser');
const JsonFind = require('json-find');
const cheerio = require('cheerio');
let jsdom = require('jsdom').JSDOM;
const dbconn = require('./db.js');
var scrape = require('html-metadata');
const logger = require("./logger.js");
//const scriptParser2 = require('./scriptParser2.js');

module.exports.parseHTML = function (d_id, url_id, html) {

    console.log("Entered into Script parser");

    logger.info("Entered into script parser and Started" + new Date().toJSON());

    dom = new jsdom(html);
        window = dom.window;
        var count = window.document.querySelectorAll('script[type="application/ld+json"]').length;
        var a="";
        for (var i = 0; i<count; i++) {
            var el = window.document.querySelectorAll('script[type="application/ld+json"]')[i];
            a += (el.innerHTML+",");
            }

                    var b = "["+a+"]";

                 var parsedJsons = b.substring(0, b.length - 2)+ ']';

             try {
                         var doc = JsonFind(JSON.parse(parsedJsons));
                    } catch (e) {
                        console.log(e);
                    }
                    try{
                        var author = doc.checkKey('author');
                        var authorName ="";
                        if(author==true) {
                            authorName = author.name;
                        }
                        else if(author==false){
                            authorName = doc.checkKey('author');
                        }

                        var founder = doc.checkKey('founder');

                        var founderName = "";
                        if (founder == true) {
                            founderName = founder.name;
                        } else if (founder == false) {
                            founderName = doc.checkKey('founder');
                        }
                    }
                    catch(e)
                    {
                        console.log(e);
                    }
                            try
                            {
                                dbconn.query(`INSERT INTO script(d_id,url_id,name, type,founder,social_info,logo,telephone,email,authorName,featureList,softwareVersion,appCategory,image,alternateName,address,addressLocality,addressRegion,contactOption,releaseNotes) VALUES('${d_id}',
                            '${url_id}','${doc.checkKey('name')}','${doc.checkKey('@type')}','${founderName}',
                            '${doc.checkKey('sameAs')}','${doc.checkKey('logo')}','${doc.checkKey('telephone')}','${doc.checkKey('email')}','${authorName}','${doc.checkKey('featureList')}',
                            '${doc.checkKey('softwareVersion')}','${doc.checkKey('applicationCategory')}','${doc.checkKey('image')}','${doc.checkKey('alternateName')}','${doc.checkKey('address')}',
                            '${doc.checkKey('addressLocality')}','${doc.checkKey('addressRegion')}','${doc.checkKey('contactOption')}','${doc.checkKey('releaseNotes')}')`, function (err, result) {
                                    if (err) throw err;
                                    console.log("JSONLD Inserted..."+result.insertId);
                                });
                            }
                            catch(e)
                            {
                                console.log(e);
                            }

//                            else
//                            {
//                                console.log("Not Found");
//                            }

        }
