const log4js = require('log4js');
log4js.configure({
    appenders: { cheese: { type: 'file', filename: '/home/agile/Desktop/logger/cheese.log' } },
    categories: { default: { appenders: ['cheese'], level: 'ALL' } }
});

const log = log4js.getLogger('cheese');

module.exports = log;

