const parser = require('./parser');
const dbconn = require('./db.js');

const cheerio =require('cheerio');
const logger = require("./logger.js");

const timeoutScheduled = Date.now();


module.exports.parseHTML = function(d_id,url_id, html) {
    //logger.info("Entered into Head parser and Started"+ new Date().toJSON());

    const $ = cheerio.load(html);
    var title = $('head title').text();
    var description = $('meta[name="description"]').attr('content');
    var Keywords = $('meta[name="Keywords"]').attr('content');
    if (title == null) {
        title=null;
    }
    if (description == null) {
        description=null;
    }
    if (Keywords == null) {
        Keywords=null;
    }
    console.log(d_id+"id isss-"+title + "  is ----- " + description + "    is -------- " + Keywords);
    try {
        dbconn.query(`INSERT into head(d_id,url_id, title, description, keywords) values("${d_id}","${url_id}","${title}", "${description}", "${Keywords}")`, function (err, result) {
            //if (err) throw err;
            console.log("Inserted into head parser");
        });
    }catch(err){
        console.log('head message  ---'+err.message);
    }
    logger.info("For head Parser Executions", timeoutScheduled - Date.now());

}
