const parser = require('./parser');
const dbconn = require('./db.js');
let jsdom = require('jsdom').JSDOM;
const jsonfind=require('json-find');
const cheerio =require('cheerio');
const logger = require("./logger.js");

const timeoutScheduled = Date.now();


module.exports.parseHTML = function(d_id,url_id, html) {

    console.log("Entered into script source url");
    logger.info("Entered into script source parser and Started"+ new Date().toJSON());

    const $ = cheerio.load(html);
    dom = new jsdom(html);
    window = dom.window;
    var m=[];
    var count=$('script').length;
    //console.log("count is   " +count);

    for (var  i = 0; i < count ; i++) {
        var  el = window.document.querySelectorAll('script')[i];
        m.push(el['src']);
    }
    var myArrayNew = m.filter(function (el) {
        return el != null && el != "";
    });

    //console.log(myArrayNew);
    for( var j=0;j<=myArrayNew.length;j++)
    {
        try {
            dbconn.query(`INSERT into scriptsourceurl(d_id, url_id, script_sourceurl) values('${d_id}','${url_id}','${myArrayNew[j]}')`, function (err, result) {
                console.log("inserted source url parser");
            });
        }catch(err){
            console.log('script error message ---'+err.message);
        }
    }
    logger.info("For Script Source Url Executions", timeoutScheduled - Date.now());

}
