
const JsonFind = require('json-find');

var a = [ { '@context': 'http://schema.org',
    '@type': 'SoftwareApplication',
    name: 'CRM Software',
    description:
     'Agile CRM Software is the best, easy, powerful and affordable Customer Relationship Management (CRM) with sales and marketing automation for small businesses.',
    image: 'https://www.agilecrm.com/img/dashboard-new/dashboard.png',
    url: 'https://www.agilecrm.com/',
    applicationCategory: 'CRM',
    operatingSystem: 'Windows, Mac, Linux, Android, IOS',
    softwareVersion: '56.8',
    screenshot: 'https://www.agilecrm.com/img/dashboard-new/dashboard.png',
    'featureList ':
     'CRM, Marketing CRM, Sales CRM, Support, Help Desk Software, Marketing Automation, Landing Pages, Web Popups, Exit Intent etc',
    releaseNotes: 'https://www.agilecrm.com/product-updates',
    offers:
     { '@type': 'Offer',
       price: '0',

       priceCurrency: 'USD',
       name: 'Free for 10 Users',
       url: 'https://www.agilecrm.com/pricing' },
    aggregateRating:
     { '@type': 'AggregateRating',
       ratingValue: '4.5',
       reviewCount: '495' },
    author: { '@type': 'Organization', name: 'Agile CRM' } },
  { '@context': 'http://schema.org',
    '@type': 'Organization',
    name: 'Agile CRM',
    url: 'https://www.agilecrm.com/',
    logo: 'https://www.agilecrm.com/img/agile-crmlogo.png',
    address:
     'Address: 12300, Ford Road, Street B306, Dallas, TX 75234, United States',
    telephone: '+1 800-980-0729',
    email: 'sales@agilecrm.com',
    sameAs:
     [ 'https://www.facebook.com/CRM.Agile',
       'https://twitter.com/agilecrm',
       'https://plus.google.com/+Agilecrm',
       'http://www.linkedin.com/company/agile-crm' ] } ]


var doc = JsonFind(a);
  console.log(doc.checkKey('name'));

