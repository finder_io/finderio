//var cheerio = require('cheerio');
var http = require('http');

var request = require('request');
var extractDomain = require('extract-domain');
var headerParser = require('./headerParser.js');
var headParser = require('./headParser.js');
var scriptsourceparser = require('./scriptSourceURLParser.js');
const dbconn = require('./db.js');
var rp = require('request-promise');
var scriptParser = require('./scriptParser.js');
var bodyParser = require('./bodyParser.js');
var emailParser = require('./emailParser.js');
const keywordParser = require('./keywordParser.js');
const logger = require("./logger.js");

// http.createServer(function (req, res) {
//     res.writeHead(200, {'Content-Type': 'text/plain'});
//     res.write('Finder Io is Running');
//     res.end();

    var timeoutScheduled = Date.now();
    console.log(timeoutScheduled);
    var exec = 0;

    logger.info("Parser Started ..", new Date().toJSON());
    dbconn.query("SELECT * FROM url_test;", function (err, result) {
        try {
            for (let i = 0; i < result.length; i++) {
                getContent(result[i]);
            }
        } catch (err) {
            console.log('total msg' + err.message);
        }
    });
    function getContent(resultObj) {
        var url = resultObj.url;
        var d_id = resultObj.d_id;
        var url_id = resultObj.id;

        request(url, function (err, resp, html) {
            console.log(`Response received for the URL ${url} is ---`);

            exec++;
            if (!err) {
                headParser.parseHTML(d_id, url_id, html);
                bodyParser.parseHTML(d_id, url_id, html, url);
                //emailParser.parseHTML(d_id, url_id, url);
                keywordParser.parseHTML(d_id, url_id, html);
                 headerParser.parseHTML(d_id, url_id, resp);
                 scriptsourceparser.parseHTML(d_id, url_id, html);
                 scriptParser.parseHTML(d_id, url_id, html);

                console.log("Requested........" + exec);
            }
            if (exec % 1000 == 0) {
                logger.info("For 1000 Executions", timeoutScheduled - Date.now());
                console.log("Timer2222222222222222222222222........" + Date.now());
                console.log("For 100 Executions", timeoutScheduled - Date.now());
            }
        });

    }

//}).listen(8030);

