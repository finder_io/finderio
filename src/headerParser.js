const parser = require('./parser');
const JsonFind = require('json-find');

const dbconn = require('./db.js')
const logger = require("./logger.js");


module.exports.parseHTML = function (d_id, url_id, resp) {
    console.log("Entered Into header parser");

    logger.info("Entered into Header parser and Started"+ new Date().toJSON());

    var freshList = "";
    var unknownList = "";

    function func() {
        dbconn.query(`select category_key from cons_variables_list where category = "header" AND type="ignorable"`, function (err, result) {

            for (let i = 0; i < result.length; i++) {
                delete resp.headers[result[i].category_key];

            }
            freshList = (resp.headers);
            knownKeysGetter(freshList);
            unknown();
        });
    }
    func();

    function knownKeysGetter(val) {
        try {
            dbconn.query('select category_key from cons_variables_list where category = "header" AND type="known"', function (err, result) {
                var doc = JsonFind((val));
                for (let i = 0; i < result.length; i++) {


                    dbconn.query(`INSERT INTO header_known(d_id,url_id,header_key, header_value) values("${d_id}","${url_id}","${result[i].category_key}","${doc.checkKey(result[i].category_key)}" )`, function (err, results) {
                        console.log("known Inserted");
                    });
                }
            });
        } catch (e) {
            console.log(e.message);
        }
    }

    function unknown() {
        try
        {
            dbconn.query('select category_key from finderio_test.cons_variables_list where category="header";', function (err, result) {

                for (let i = 0; i < result.length; i++) {

                    delete resp.headers[result[i].category_key];
                }
                unknownList = (resp.headers);

                var doc = JsonFind(unknownList);

                var keys = Object.keys(unknownList);

                for (let i = 0; i < keys.length; i++) {
                    dbconn.query(`INSERT INTO header_unknown(d_id,url_id,header_key, header_value) values("${d_id}","${url_id}","${keys[i]}","${doc.checkKey(keys[i])}" )`, function (err, results) {
                        console.log("unknown Inserted");
                    });
                }
            });

        } catch (e) {
            console.log(e);
        }
    }
}




